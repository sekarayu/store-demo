Feature: Sample Page

  Scenario: Check comment validation
    Given the user open home page
    When the user go to Sample Page menu
    And the user fill comment box "comment with wrong email"
    And the user fill name "test123"
    And the user fill email "abc.mail.com"
    And the user posts comment
    And error msg "invalid email" is displayed
    And the user click back button from error page
    And the user fill comment box "comment with correct email"
    And the user fill name "test123"
    And the user fill email "abc@mail.com"
    And the user posts comment
    Then verify comment is received