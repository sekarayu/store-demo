package com.adidas.test;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

/**
 * Created by sekarayu on 12/08/2018.
 */
@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features="src/test/resources/features",
        plugin = {"pretty"}
)
public class TestsRunner {
}
