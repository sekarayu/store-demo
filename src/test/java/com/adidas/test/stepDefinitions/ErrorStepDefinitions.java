package com.adidas.test.stepDefinitions;

import com.adidas.test.steps.ErrorSteps;
import com.adidas.test.steps.SampleSteps;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

/**
 * Created by sekarayu on 14/08/2018.
 */
public class ErrorStepDefinitions {

    @Steps
    public ErrorSteps errorSteps;

    @Steps
    public SampleSteps sampleSteps;

    @And("^error msg \"([^\"]*)\" is displayed$")
    public void error_msg_is_displayed(String arg1) throws Throwable {
        switch (arg1){
            case "invalid email" : errorSteps.validateErrorMsgInvEmail();break;
        }
    }

    @And("^the user click back button from error page$")
    public void the_user_click_back_button_from_error_page() throws Throwable {
        errorSteps.clickBackBtn();
        sampleSteps.verifyOnSamplePage();
    }
}
