package com.adidas.test.stepDefinitions;

import com.adidas.test.steps.HomeSteps;
import com.adidas.test.steps.SampleSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

/**
 * Created by sekarayu on 13/08/2018.
 */
public class HomeStepDefinitions {

    @Steps
    public HomeSteps homeSteps;

    @Steps
    public SampleSteps sampleSteps;

    @Given("^the user open home page$")
    public void theUserOpenHomePage() throws Throwable {
        homeSteps.openPage();
    }

    @When("^the user go to Sample Page menu")
    public void theUserGoToSamplePageMenu() throws Throwable{
        homeSteps.clickSamplePage();
        sampleSteps.verifyOnSamplePage();
    }
}
