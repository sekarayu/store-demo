package com.adidas.test.stepDefinitions;

import com.adidas.test.steps.SampleSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

/**
 * Created by sekarayu on 14/08/2018.
 */
public class SampleStepDefinitions {

    @Steps
    public SampleSteps sampleSteps;

    @And("^the user fill comment box \"([^\"]*)\"$")
    public void the_user_fill_comment_box(String arg1) throws Throwable{
        sampleSteps.fillComment(arg1);
    }

    @And("^the user fill name \"([^\"]*)\"$")
    public void the_user_fill_name(String arg1) throws Throwable {
        sampleSteps.fillName(arg1);
    }

    @And("^the user fill email \"([^\"]*)\"$")
    public void the_user_fill_email(String arg1) throws Throwable {
        sampleSteps.fillEmail(arg1);
    }

    @And("^the user posts comment$")
    public void the_user_posts_comment() throws Throwable {
        sampleSteps.clickPostCommentBtn();
    }

    @Then("^verify comment is received$")
    public void verify_comment_is_received() throws Throwable {
        sampleSteps.verifyCommentIsReceived();
    }
}
