package com.adidas.test.steps;

import com.adidas.test.pages.HomePage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by sekarayu on 13/08/2018.
 */
public class HomeSteps {
    static HomePage homePage = new HomePage();

    @Step
    public static void openPage(){
        homePage.getDriver().manage().window().maximize();
        homePage.getDriver().get(homePage.URL);
        checkHomePage();
    }

    private static void checkHomePage(){
        WebDriverWait wait = new WebDriverWait(homePage.getDriver(),10);
        wait.until(ExpectedConditions.visibilityOf(homePage.logo));
        Assert.assertTrue(homePage.logo.isDisplayed());
        Assert.assertTrue(homePage.cart.isDisplayed());
        Assert.assertTrue(homePage.account.isDisplayed());
    }

    @Step
    public static void clickSamplePage(){
        ((JavascriptExecutor) homePage.getDriver()).executeScript("arguments[0].scrollIntoView(true);", homePage.samplePage);
        homePage.samplePage.click();
    }
}
