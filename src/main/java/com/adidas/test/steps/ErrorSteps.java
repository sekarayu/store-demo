package com.adidas.test.steps;

import com.adidas.test.pages.ErrorPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

/**
 * Created by sekarayu on 14/08/2018.
 */
public class ErrorSteps {

    static ErrorPage errorPage = new ErrorPage();

    @Step
    public static void validateErrorMsgInvEmail(){
        Assert.assertTrue("Error msg is not displayed!",errorPage.errorMsg.isDisplayed());
        String msg = errorPage.errorMsg.getText();
        Assert.assertTrue("Error msg email invalid is displayed!",msg.contains(errorPage.errorEmailNotValidMsg));
    }

    @Step
    public static void clickBackBtn(){
        errorPage.backBtn.click();
    }
}
