package com.adidas.test.steps;

import com.adidas.test.pages.SamplePage;
import net.thucydides.core.annotations.Step;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.util.Arrays;

/**
 * Created by sekarayu on 14/08/2018.
 */
public class SampleSteps {

    static SamplePage samplePage = new SamplePage();
    final static Logger logger = Logger.getLogger(SampleSteps.class);
    static String COMMENT = "";
    static String NAME = "";

    @Step
    public static void verifyOnSamplePage(){
        Assert.assertTrue("Not in Sample Page",samplePage.commentBox.isDisplayed());
    }

    @Step
    public static void fillComment(String cmt){
        //add random string to avoid duplication
        RandomStringUtils randomStringUtils = new RandomStringUtils();
        cmt = cmt + randomStringUtils.randomAlphanumeric(3);
        samplePage.commentBox.type(cmt);
        COMMENT = cmt;
    }

    @Step
    public static void fillEmail(String eml){
        samplePage.email.type(eml);
    }

    @Step
    public static void fillName(String name){
        samplePage.name.type(name);
        NAME = name;
    }

    @Step
    public static void clickPostCommentBtn(){
        samplePage.postCommentBtn.click();
    }

    @Step
    public static void verifyCommentIsReceived(){
        Assert.assertTrue("Comment is not displayed",samplePage.commentBody.isDisplayed());
        //list of comments
        int count = samplePage.getDriver().findElements(By.className("comment-body")).size();
        String[] comments = new String[count];
        logger.info("count = "+count);
        for(int i=0;i<count;i++){
            comments[i] = samplePage.getDriver().findElements(By.className("comment-body")).get(i).getText();
            logger.info("comm "+i+" = "+comments[i]);
        }
        Assert.assertTrue(Arrays.asList(comments).contains(COMMENT));
    }
}
