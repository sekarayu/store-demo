package com.adidas.test.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

/**
 * Created by sekarayu on 14/08/2018.
 */
public class ErrorPage extends PageObject {

    @FindBy(xpath = "//*[@id=\"error-page\"]/p[2]")
    public static WebElementFacade errorMsg;

    public static String errorEmailNotValidMsg = "please enter a valid email address";

    @FindBy(xpath = "//*[@id=\"error-page\"]/p[4]/a")
    public static WebElementFacade backBtn;
}
