package com.adidas.test.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

/**
 * Created by sekarayu on 13/08/2018.
 */
public class HomePage extends PageObject {
    public static String URL = "http://store.demoqa.com/";

    @FindBy(id = "logo")
    public static WebElementFacade logo;

    @FindBy(id = "header_cart")
    public static WebElementFacade cart;

    @FindBy(id = "account")
    public static WebElementFacade account;

    @FindBy(linkText = "Sample Page")
    public static WebElementFacade samplePage;
}
