package com.adidas.test.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

/**
 * Created by sekarayu on 14/08/2018.
 */
public class SamplePage extends PageObject {

    @FindBy(id = "comment")
    public static WebElementFacade commentBox;

    @FindBy(id = "author")
    public static WebElementFacade name;

    @FindBy(id = "email")
    public static WebElementFacade email;

    @FindBy(id = "url")
    public static WebElementFacade website;

    @FindBy(id = "submit")
    public static WebElementFacade postCommentBtn;

    @FindBy(className = "comment-body")
    public static WebElementFacade commentBody;
}
