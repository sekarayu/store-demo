#Pre-requisite Tools:
-    Apache Maven
-    JDK 8

#Project Structure
-	driver : consist of chromedriver
-	pom.xml
-	src/main/java/com/adidas/test/pages : list of all UI elements used for testing
-	src/main/java/com/adidas/test/steps : list of all actions to be done for elements listed in "pages"
-	src/main/resources/log4j.xml : logging properties
-	src/test/java/com/adidas/test/TestsRunner.java : Test runner
-	src/test/java/com/adidas/test/stepDefinitions : consist of classes that can be used to map the story (scenario) in feature file with "steps" classes.
-	src/test/resources/features/SamplePage.feature : cucumber feature file
-	src/test/resources/serenity.properties : List all Serenity properties needed

#Note:
-    I used Serenity BDD (Thucydides) combine with Cucumber to implement PageObjects and it will create good graphical report. You can find anything about Serenity here : http://www.thucydides.info/#/
-    As for the programming language, I used Java and Apache Maven as build tool.
-    The test will be run in Chrome browser. Please check the pom.xml : <webdriver.driver>chrome</webdriver.driver> <webdriver.chrome.driver>${chromedriver}</webdriver.chrome.driver>
-    Chromedriver is located inside "driver" folder.
-	 There are 2 Chromedriver inside "driver" folder. One for Windows environment (chromedriver.exe) and the other one for Mac environment (chromedriver).
-	 The default Chromedriver in the pom.xml is for Mac. If you want to change it for Windows, change this line inside "properties" tag to : <chromedriver>${basedir}/driver/chromedriver.exe</chromedriver>
-    To run the test : point to the project directory (where the pom.xml exist) - type "mvn clean install" and enter
-    To check the application log : application.log
-    To check the report : /target/site/serenity/index.html